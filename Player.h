#pragma once

#include "SpriteSheetAnimation.h"
#include "Entity.h"
#include "Sound.h"


#define ScreenWidth 800
#define ScreenHeight 608

class Player : public Entity
{
private:
	ALLEGRO_BITMAP *playerImage;

	ALLEGRO_TRANSFORM camera;

	std::pair<float, float> cameraPosition;
	std::pair<float, float> groundPosition;
	float moveSpeed;

	SpriteSheetAnimation ssAnimation;

	enum Direction {Left = 1, Right = 0, Down = 2, Up = 3};

	std::pair<Direction, Direction> direction; // direction.first = current direction, direction.second = old direction

	//bool marioBig; // true = mario is big, false = mario is small

	float jumpSpeed;

	Animation MARIO;
	Animation SWIAT;
	Animation CZAS;
	Animation PUNKTY;
	Animation SWIAT2;
	Animation CZAS2;

	bool keyPressed;
	bool jump;

public:
	Player(void);
	~Player(void);

	void LoadContent(std::vector<std::string> attributes, std::vector<std::string> contents, std::string type);
	void UnloadContent();
	void Update(ALLEGRO_EVENT ev, InputManager input, std::pair<float, float> enemyPosition);
	void TopCollision(Entity e2);
	void OtherCollision(Entity e2);
	void Draw(ALLEGRO_DISPLAY *display);
	void CameraUpdate(std::pair<float, float> position, int playerWidth, int playerHeight);
};