#pragma once

#include <allegro5\allegro.h>
#include <vector>
#include <string>
#include <iostream>

#include "InputManager.h"
#include "Animation.h"
#include "SpriteSheetAnimation.h"
#include "FloatRect.h"

class Entity
{
public:
	Entity(void);
	~Entity(void);

	virtual void LoadContent(std::vector<std::string> attributes, std::vector<std::string> contents, std::string type);
	virtual void UnloadContent();
	virtual void Update(ALLEGRO_EVENT ev, InputManager inputManager, std::pair<float, float> enemyPosition);
	virtual void TopCollision(Entity e2);
	virtual void OtherCollision(Entity e2);
	virtual void Draw(ALLEGRO_DISPLAY *display);

	std::pair<float, float> position, prevPosition, velocity;
	Animation animation;
	bool activateGravity;
	bool containsEntity;

	FloatRect *rect, *prevRect;

	enum Direction {Left = 1, Right = 0, Down = 2, Up = 1};
	std::pair<int, int> direction;
	int tileID;

	int czas;
	int monety;
	int punkty;
	std::string swiat;
	std::string tempString;
	bool isDead;
	bool deleteTile;
	int deleteTileCounter;

	// Tylko do gracza
	bool bigMario;
	int playerWidth;
	int playerHeight;

	friend class Tile;

protected:
	ALLEGRO_BITMAP *image;
	ALLEGRO_BITMAP *alternativeImage;
	float gravity;
	float moveSpeed;
	int range;
	SpriteSheetAnimation ssAnimation;
};

