#pragma once

#include "GameScreen.h"
#include "Map.h"
#include "EntityManager.h"
#include "ScreenManager.h"
#include "Sound.h"

class GameplayScreen : public GameScreen
{
private:
	Map map;
	EntityManager player, enemies;

	int timerCount;
public:
	GameplayScreen(void);
	~GameplayScreen(void);

	void LoadContent();
	void UnloadContent();
	void Update(ALLEGRO_EVENT ev);
	void Draw(ALLEGRO_DISPLAY *display);
};