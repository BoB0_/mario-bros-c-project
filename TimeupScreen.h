#pragma once

#include "ScreenManager.h"
#include "FileManager.h"

class TimeupScreen : public GameScreen
{
public:
	TimeupScreen(void);
	~TimeupScreen(void);

	void LoadContent();
	void UnloadContent();
	void Update(ALLEGRO_EVENT ev);
	void Draw(ALLEGRO_DISPLAY *display);

private:
	std::vector<ALLEGRO_BITMAP*> images;
	std::vector<Animation*> fade;

	ALLEGRO_TRANSFORM camera;
};

