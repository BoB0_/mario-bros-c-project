#include "EntityManager.h"


EntityManager::EntityManager(void)
{
}


EntityManager::~EntityManager(void)
{
}

void EntityManager::LoadContent(std::string filename, std::string id, std::string type)
{
	std::vector<std::vector<std::string>> attributes;
	std::vector<std::vector<std::string>> contents;
	FileManager fileManager;
	if(id == "")
		fileManager.LoadContent(filename.c_str(), attributes, contents);
	else
		fileManager.LoadContent(filename.c_str(), attributes, contents, id);

	for(int i = 0; i < attributes.size(); i++)
	{
		Entity *entity;
		if(type == "Player")
			entity = new Player();
		else if(type == "Enemy")
			entity = new Enemy();

		entities.push_back(entity);
		if(type == "Player")
			entities[i]->LoadContent(attributes[i], contents[i], type);
		else if(type == "Enemy")
			entities[i]->LoadContent(attributes[i], contents[i], type);

		//entities[i]->LoadContent(attributes[i], contents[i]);
	}
}

void EntityManager::UnloadContent()
{
	for(int i = 0; i < entities.size(); i++)
	{
		entities[i]->UnloadContent();
		delete entities[i];
	}
}

void EntityManager::Update(ALLEGRO_EVENT ev, InputManager input, std::pair<float, float> enemyPosition)
{
		for(int i = 0; i < entities.size(); i++)
		{
			entities[i]->Update(ev, input, enemyPosition);
			if(entities[i]->deleteTileCounter <= 0 && entities[i]->deleteTile)
				entities.erase(entities.begin() + i);
		}
}

void EntityManager::EntityCollision(EntityManager *e2)
{
	for( int i = 0; i < entities.size(); i++)
	{
		for( int j = 0; j < e2->entities.size(); j++)
		{
			if(entities[i]->rect->Intersects(*e2->entities[j]->rect))
			{
				if(entities[i]->rect->Bottom >= e2->entities[j]->rect->Top && entities[i]->prevRect->Bottom <= e2->entities[j]->rect->Top && !e2->entities[j]->isDead) // Top Collision
				{
					entities[i]->TopCollision(*e2->entities[j]);
					e2->entities[j]->TopCollision(*entities[i]);
					//e2->entities.erase(e2->entities.begin()+j);
				}
				else
				{
					if(!e2->entities[j]->isDead)
					{
						entities[i]->OtherCollision(*e2->entities[j]);
						e2->entities[j]->OtherCollision(*entities[i]);
					}
				}
			}
		}
	}
}

void EntityManager::Draw(ALLEGRO_DISPLAY *display)
{
	for(int i = 0; i < entities.size(); i++)
		entities[i]->Draw(display);
}
