#include "SoundManager.h"

SoundManager &SoundManager::GetInstance()
{
	static SoundManager instance;
	return instance;
}

SoundManager::SoundManager()
{
}


SoundManager::~SoundManager(void)
{
}

void SoundManager::LoadContent(std::string fileName)
{
	fileManager.LoadContent(fileName.c_str(), attributes, contents);

	samplesReserved = 1;

	for(int i = 0; i < attributes.size(); i++)
	{
		for(int j = 0; j < attributes[i].size(); j++)
		{
			/*if(attributes[i][j] == "Sound")
			{
				Sound soundInstance;
				ALLEGRO_SAMPLE *sample = al_load_sample(contents[i][j].c_str());
				sounds.push_back(soundInstance);
				sounds[sounds.size()-1].SetContent(sample);
				samplesReserved++;
			}*/
		}
	}

	al_reserve_samples(samplesReserved);
}

void SoundManager::UnloadContent()
{
	for(int i = 0; i < sounds.size(); i++)
		sounds[i].UnloadContent();
}

void SoundManager::Update()
{
}

void SoundManager::Play(int id)
{
	/*sounds[id].Play();*/
}