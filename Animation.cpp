#include "Animation.h"


Animation::Animation(void)
{
	fadeSpeed = 5.0f;
	increase = false;
}


Animation::~Animation(void)
{
}

void Animation::LoadContent(ALLEGRO_BITMAP *image, std::string text, std::pair<float,float> position)
{
	this->image = image;
	this->text = text;
	this->position = position;
	//this->type = type;

	alpha = 255;
	if(text == "PRESS ESCAPE")
		font = al_load_font("verdana.ttf", 30, NULL);
	else
		font = al_load_font("verdana.ttf", 12, NULL);
	sourceRect = image;
	isActive = false;
	amountOfFrames = std::pair<int, int>(14, 2);
	currentFrame = std::pair<int, int>(0, 0);
}

void Animation::LoadContent(ALLEGRO_BITMAP *image, std::string text, std::pair<float, float> position, std::string type)
{
	this->image = image;
	this->text = text;
	this->position = position;
	this->type = type;

	alpha = 255;
	font = al_load_font("arial.ttf", 12, NULL);
	sourceRect = image;
	isActive = false;
	if(type == "Player")
	{
		amountOfFrames = std::pair<int, int>(14, 2);
		currentFrame = std::pair<int, int>(0, 0);
	}
	else if(type == "Enemy")
	{
		amountOfFrames = std::pair<int, int>(3, 1);
		currentFrame = std::pair<int, int>(0, 0);
	}
	/*amountOfFrames = std::pair<int, int>(14, 2);
	currentFrame = std::pair<int, int>(1, 0);*/
}

void Animation::UnloadContent()
{
	al_destroy_font(font);
	alpha = NULL;
	position = std::pair<float,float>(0,0);
}

void Animation::Update()
{
	if(IsActive())
	{
		if(!increase)
			alpha -= fadeSpeed;
		else
			alpha += fadeSpeed;

		if(alpha <= 0)
		{
			alpha = 0;
			increase = true;
		}
		else if(alpha >= 255)
		{
			alpha = 255;
			increase = false;
		}
	}
	else 
		alpha = 255;

	printf("%g\n", alpha);
}

void Animation::Draw(ALLEGRO_DISPLAY *display)
{
	al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
	if(sourceRect != NULL)
		al_draw_tinted_bitmap(sourceRect, al_map_rgba(255, 255, 255, alpha),
		position.first, position.second, NULL);

	if(text != "")
		al_draw_text(font, al_map_rgba(255, 255 , 255, alpha), position.first, position.second, NULL, text.c_str());
}


float &Animation::Alpha()
{
	return this->alpha;
}

bool &Animation::IsActive()
{
	return isActive;
}

std::pair<float, float> &Animation::Position()
{
	return position;
}

std::pair<int, int> &Animation::AmountOfFrames()
{
	return amountOfFrames;
}

std::pair<int, int> &Animation::CurrentFrame()
{
	return currentFrame;
}

std::pair<int, int> Animation::FrameDimensions()
{
	std::pair<int, int> fd(al_get_bitmap_width(image) / amountOfFrames.first,
		al_get_bitmap_height(image) / amountOfFrames.second);

	return fd;
}

ALLEGRO_BITMAP *Animation::Image()
{
	return image;
}

ALLEGRO_BITMAP *&Animation::SourceRect()
{
	return sourceRect;
}

void Animation::SetIncrease(bool value)
{
	increase = value;
}

std::string &Animation::Text()
{
	return text;
}