#pragma once
class FloatRect
{
public:
	FloatRect(float x, float y, float w, float h);
	~FloatRect(void);

	bool Intersects(FloatRect f);

	const float Left, Right, Top, Bottom;
};

