#include "GameplayScreen.h"


GameplayScreen::GameplayScreen(void)
{
}


GameplayScreen::~GameplayScreen(void)
{
}

void GameplayScreen::LoadContent()
{
	map.LoadContent("Map1");
	player.LoadContent("Load/Player.rb", "", "Player");
	enemies.LoadContent("Load/Enemy.rb", "", "Enemy");
	//sound.LoadContent("Load/sounds.rb");

	Sound::GetInstance().LoadContent("Load/sounds.rb");

	timerCount = 0;


	song = al_load_sample("Sounds/background.ogg");
	songInstance = al_create_sample_instance(song);
	al_set_sample_instance_playmode(songInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(songInstance, al_get_default_mixer());
	al_play_sample_instance(songInstance);
}

void GameplayScreen::UnloadContent()
{
	GameScreen::UnloadContent();
	map.UnloadContent();
	player.UnloadContent();
	enemies.UnloadContent();

	al_destroy_sample_instance(songInstance);
}

void GameplayScreen::Update(ALLEGRO_EVENT ev)
{
	player.Update(ev, input, player.entities[0]->position);
	enemies.Update(ev, input, player.entities[0]->position);
	
	for(int i = 0; i < enemies.entities.size(); i++)
		map.Update(*enemies.entities[i]);
	for(int i = 0; i < player.entities.size(); i++)
		map.Update(*player.entities[i]);

	player.EntityCollision(&enemies);


	//Zmniejszanie czasu na przejscie mapy
	if(ev.type == ALLEGRO_EVENT_TIMER)
		timerCount++;

	if(timerCount == 15)
	{
		timerCount = 0;
		if(player.entities[0]->czas > 0)
		player.entities[0]->czas--;
	}
	//

	for(int i = 0; i < player.entities.size(); i++)
	{
		if(player.entities[i]->isDead == true || player.entities[i]->position.second >= 480 || player.entities[i]->position.first >= 3265)
		{
			Sound::GetInstance().Play(8);
			al_stop_sample_instance(songInstance);
			ScreenManager::GetInstance().AddScreen("GameOverScreen");
		}
		else if(player.entities[i]->czas <= 0)
		{
			ScreenManager::GetInstance().AddScreen("TimeupScreen");
		}
	}
}

void GameplayScreen::Draw(ALLEGRO_DISPLAY *display)
{
	map.Draw(display);
	enemies.Draw(display);
	player.Draw(display);
}