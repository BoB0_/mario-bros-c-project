#include "Layer.h"

Layer &Layer::GetInstance()
{
	static Layer instance;
	return instance;
}

Layer::Layer()
{
}


Layer::~Layer(void)
{
}

std::pair<int, int> Layer::SetTiles(std::string tileString)
{
	std::pair<int, int> tile;
	tile.first = atoi(tileString.substr(0, tileString.find(',')).c_str());
	tile.second = atoi(tileString.substr(tileString.find(',')+1).c_str());

	return tile;
}

void Layer::LoadContent(std::string layerID, std::string mapID)
{
	std::string filename = "Load/Maps/"+mapID+".txt";
	fileManager.LoadContent(filename.c_str(), attributes, contents, layerID);
	int indexY = 0;
	id = 0;
	
	for(int i = 0; i < attributes.size(); i++)
	{
		for( int j = 0; j < attributes[i].size(); j++)
		{
			if(attributes[i][j] == "SolidTiles")
				solidTiles.push_back(SetTiles(contents[i][j]));
			else if(attributes[i][j] == "TileSheet")
				tileSheet = al_load_bitmap(contents[i][j].c_str());
			else if(attributes[i][j] == "Background")
			{
				//al_set_new_bitmap_flags(ALLEGRO_MEMORY_BITMAP);
				//ALLEGRO_BITMAP *tileImage;
				backgroundImage = al_load_bitmap(contents[i][j].c_str());
				//tileImage = al_create_sub_bitmap(backgroundImage, 0, 0, al_get_bitmap_width(backgroundImage), al_get_bitmap_height(backgroundImage));
				std::pair<float, float> position(-128, 272);
				std::pair<int, int> tile(100,100);
				Tile tileInstance;
				tiles.push_back(tileInstance);
				tiles[tiles.size()-1].SetContent(backgroundImage, Tile::State::Solid, position, tileSheet, tile, id);
				id++;
				//al_set_new_bitmap_flags(ALLEGRO_VIDEO_BITMAP);
			}
			else if(attributes[i][j] == "StartLayer")
			{
				for(int k = 0; k < contents[i].size(); k++)
				{
					if(contents[i][k] != "---")
					{
						ALLEGRO_BITMAP *tileImage;
						Tile::State tempState = Tile::State::Passive;
					
						std::pair<int, int> tile = SetTiles(contents[i][k]);
						if(std::find(solidTiles.begin(), solidTiles.end(), tile)
							!= solidTiles.end())
						{
							tempState = Tile::State::Solid;
						}

						tileImage = al_create_sub_bitmap(tileSheet, tile.first*16, tile.second*16, 16, 16);

						std::pair<float, float> position(k*16, indexY*16);

						Tile tileInstance;
						tiles.push_back(tileInstance);
						tiles[tiles.size()-1].SetContent(tileImage, tempState, position, tileSheet, tile, id);
						id++;
					}
				}

				indexY++;
			}
		}
	}
}

void Layer::UnloadContent()
{
	for(int i = 0; i < tiles.size(); i++)
		tiles[i].UnloadContent();

	al_destroy_bitmap(tileSheet);
	//al_destroy_bitmap(backgroundImage);
}

void Layer::Update(Entity &e)
{
	for(int i = 0; i < tiles.size(); i++)
		tiles[i].Update(e);
}

void Layer::Draw(ALLEGRO_DISPLAY *display)
{
	for(int i = 0; i < tiles.size(); i++)
	{
		tiles[i].Draw(display);
	}

}

void Layer::AddTile(ALLEGRO_BITMAP *tileImage, Tile::State state, std::pair<float, float> position, 
	ALLEGRO_BITMAP *tileSheet, std::pair<int, int> tile)
{
	Tile tileInstance;
	tiles.push_back(tileInstance);
	tiles[tiles.size()-1].SetContent(tileImage, state, position, tileSheet, tile, id);
	id++;
}