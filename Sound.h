#pragma once

#include <allegro5\allegro.h>
#include <allegro5\allegro_audio.h>
#include <allegro5\allegro_acodec.h>
#include <string>

#include "FileManager.h"

class Sound
{
public:
	static Sound &GetInstance();
	~Sound(void);

	void LoadContent(std::string fileName);
	void UnloadContent();
	void Update(ALLEGRO_EVENT ev);
	void Play(int id);

private:
	Sound();
	Sound(Sound const&);
	ALLEGRO_SAMPLE *sound;
	ALLEGRO_SAMPLE_ID sampleID;
	FileManager fileManager;

	std::vector<std::vector<std::string>> attributes;
	std::vector<std::vector<std::string>> contents;
	std::vector<ALLEGRO_SAMPLE*> sounds;

	int samplesReserved;
	int playingSample;
};

