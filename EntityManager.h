#pragma once

#include "Entity.h"
#include "FileManager.h"
#include "Player.h"
#include "Enemy.h"

class EntityManager
{
public:
	EntityManager(void);
	~EntityManager(void);

	void LoadContent(std::string filename, std::string id, std::string type);
	void UnloadContent();
	void Update(ALLEGRO_EVENT ev, InputManager input, std::pair<float, float> enemyPosition);
	void EntityCollision(EntityManager *e2);
	void Draw(ALLEGRO_DISPLAY *display);

	std::vector<Entity*> entities;
};

