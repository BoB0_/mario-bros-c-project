#include "TileAnimation.h"


TileAnimation::TileAnimation(void)
{
	frameCounter = 0;
	switchFrame = 5;
	frame = 1;
}


TileAnimation::~TileAnimation(void)
{
}


void TileAnimation::Update(Tile tile)
{
	/*if(tile.tileAnimation.IsActive())
	{
		frameCounter++;
		if(frameCounter >= switchFrame)
		{
			frameCounter = 0;
			frame++;
			tile.tileAnimation.CurrentFrame().first = tile.NumbersOfTiles()[frame];
		}
	}
	else
	{
		if(tile.NumbersOfTiles()[4] == 1)
		{
			frameCounter = 0;
			tile.tileAnimation.CurrentFrame().first = tile.NumbersOfTiles()[5];
		}
	}
	
	tile.tileAnimation.SourceRect() = al_create_sub_bitmap(tile.tileAnimation.Image(), tile.tileAnimation.CurrentFrame().first * 16, tile.tileAnimation.CurrentFrame().second * 16, 16, 16);

*/
	if(tile.tileAnimation.IsActive())
	{
		if(tile.NumbersOfTiles()[0] == 0)
			tile.tileAnimation.CurrentFrame().first = 1;
		else
		{
			frameCounter++;
			if(frameCounter >= switchFrame)
			{
				frameCounter = 0;
				tile.tileAnimation.CurrentFrame().first = tile.NumbersOfTiles()[frame];
				if(frame++ >= 3)
					frame = 1;
			}
		}
	}
	else
	{
		if(tile.NumbersOfTiles()[4] == 1)
		{
			frameCounter = 0;
			tile.tileAnimation.CurrentFrame().first = tile.NumbersOfTiles()[5];
		}
		else
			tile.tileAnimation.CurrentFrame().first = 1;
	}
	tile.tileAnimation.SourceRect() = al_create_sub_bitmap(tile.tileAnimation.Image(), tile.tileAnimation.CurrentFrame().first * 16, tile.tileAnimation.CurrentFrame().second * 16, 16, 16);
}
