#include "TimeupScreen.h"


TimeupScreen::TimeupScreen(void)
{
}


TimeupScreen::~TimeupScreen(void)
{
}


void TimeupScreen::LoadContent()
{
	fileManager.LoadContent("Load/timeup.rb", attributes, contents);

	for(int i = 0; i < attributes.size(); i++)
	{
		for(int j = 0; j < attributes[i].size(); j++)
		{
			if(attributes[i][j] == "Image")
			{
				std::pair<float, float> position(0,0);
				images.push_back(al_load_bitmap(contents[i][j].c_str()));
				fade.push_back(new Animation);
				fade[fade.size()-1]->LoadContent(images[fade.size()-1], "", position);
				fade[fade.size()-1]->IsActive() = false;
			}
			else if(attributes[i][j] == "Text")
			{
				std::pair<float, float> position (275, 441);
				images.push_back(al_load_bitmap(contents[i][j].c_str()));
				fade.push_back(new Animation);
				std::string text = contents[i][j].c_str();
				fade[fade.size()-1]->LoadContent(NULL, text, position);
				fade[fade.size()-1]->IsActive() = true;
			}
		}
	}

	al_identity_transform(&camera);
	al_invert_transform(&camera);
	al_translate_transform(&camera, 0,0);
	al_use_transform(&camera);
}

void TimeupScreen::UnloadContent()
{
	GameScreen::UnloadContent();
	for(int i = 0; i < fade.size(); i++)
	{
		al_destroy_bitmap(images[i]);
		delete fade[i];
	}
	fade.clear();
}

void TimeupScreen::Update(ALLEGRO_EVENT ev)
{
	fade[fade.size()-1]->Update();

	if(fade[fade.size()-1]->Alpha() == 0)
	{
		fade[fade.size()-1]->Alpha() = 255;
	}
}

void TimeupScreen::Draw(ALLEGRO_DISPLAY *display)
{
	//fade[0]->Draw(display);
	for(int i = 0; i < fade.size(); i++)
	{
		fade[i]->Draw(display);
	}
}