#pragma once

#include "Tile.h"
#include "FileManager.h"
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iostream>

class Layer
{
public:
	static Layer &GetInstance();
	~Layer(void);

	void LoadContent(std::string layerID, std::string mapID);
	void UnloadContent();
	void Update(Entity &e);
	void Draw(ALLEGRO_DISPLAY *display);
	void AddTile(ALLEGRO_BITMAP *tileImage, Tile::State state, std::pair<float, float> position, 
		ALLEGRO_BITMAP *tileSheet, std::pair<int, int> tile);

	friend class Tile;

private:
	Layer();
	Layer(Layer const&);
	FileManager fileManager;

	std::vector<std::vector<std::string>> attributes;
	std::vector<std::vector<std::string>> contents;
	std::vector<std::pair<int, int>> solidTiles;
	std::vector<Tile> tiles;

	std::pair<int, int> SetTiles(std::string tileString);

	ALLEGRO_BITMAP *tileSheet;
	ALLEGRO_BITMAP *backgroundImage;

	int id;
};