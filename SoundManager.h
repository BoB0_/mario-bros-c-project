#pragma once

#include <string>

#include "FileManager.h"
#include "Sound.h"

class SoundManager
{
public:
	static SoundManager &GetInstance();
	~SoundManager(void);

	void LoadContent(std::string fileName);
	void UnloadContent();
	void Update();
	void Play(int id);

private:
	SoundManager();
	FileManager fileManager;

	std::vector<std::vector<std::string>> attributes;
	std::vector<std::vector<std::string>> contents;
	std::vector<Sound> sounds;

	int samplesReserved;
};

