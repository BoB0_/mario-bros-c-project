#include "Tile.h"
#include "Layer.h"


Tile::Tile(void)
{
}


Tile::~Tile(void)
{
}

void Tile::SetContent(ALLEGRO_BITMAP *tileImage, State state, std::pair<float, float> position, ALLEGRO_BITMAP *tileSheet, std::pair<int, int> tile, int id)
{
	image = tileImage;
	this->state = state;
	this->position = position;
	this->tile = tile;
	this->tileSheet = tileSheet;
	this->id = id;
	if(tile.first == 8)
		collisionIterator = 9;
	this->increasePosition = false;
	this->increasePositionIterator = 0;
	tileAnimation.LoadContent(tileImage, "", position);
}

void Tile::UnloadContent()
{
	al_destroy_bitmap(image);
}

void Tile::Update(Entity &e)
{
	if(increasePosition)
	{
		if(tile.first == 16 || tile.first == 17 || tile.first == 18 || tile.first == 19)
		{
			if(increasePositionIterator <= 100)
			{
				position.second -= 0.5;
				increasePositionIterator++;
				//tileAnimation.Position() = position;
				if(increasePositionIterator % 10 == 0)
				{
					tile.first++;
					image = al_create_sub_bitmap(tileSheet, tile.first*16, tile.second*16, 16, 16);
					tileAnimation.LoadContent(image, "", position);
					if(tile.first >= 19)
						tile.first = 16;
				}
			}
			else if(increasePositionIterator < 200)
			{
				position.second += 0.5;
				increasePositionIterator++;
				//tileAnimation.Position() = position;
				if(increasePositionIterator % 10 == 0)
				{
					tile.first++;
					image = al_create_sub_bitmap(tileSheet, tile.first*16, tile.second*16, 16, 16);
					tileAnimation.LoadContent(image, "", position);
					if(tile.first >= 19)
						tile.first = 16;
				}
			}
			else if(increasePositionIterator == 200)
			{
				image = al_create_sub_bitmap(tileSheet, 7*16, tile.second*16, 16, 14);
				state = State::Passive;
				tile.first = 7;
				tileAnimation.LoadContent(image, "", position);
			}
			printf("\n\t\t\t\t\t\t%g\t%g", position.first, position.second);
		}
		else if(tile.first == 1)
		{
			if(increasePositionIterator <= 50)
			{
				position.second -= 0.2;
				increasePositionIterator++;
				tileAnimation.Position() = position;
			}
			else if(increasePositionIterator <= 101)
			{
				position.second += 0.2;
				increasePositionIterator++;
				tileAnimation.Position() = position;
			}
			else if(increasePositionIterator == 102)
			{
				increasePosition = false;
				increasePositionIterator = 0;
			}
		}
		else if(tile.first == 20)
		{
			if(increasePositionIterator <= 50)
			{
				position.second -= 0.15;
				increasePositionIterator++;
				tileAnimation.Position() = position;
			}
			else if(increasePositionIterator <= 101)
			{
				position.second += 0.15;
				increasePositionIterator++;
				tileAnimation.Position() = position;
			}
			else if(increasePositionIterator == 102)
			{
				increasePosition = false;
				increasePositionIterator = 0;
				state = State::Solid;
			}
		}
		else if(tile.first == 10)
		{
			if(increasePositionIterator <= 50)
			{
				position.second -= 0.15;
				increasePositionIterator++;
				tileAnimation.Position() = position;
			}
			else if(increasePositionIterator <= 101)
			{
				position.second += 0.15;
				increasePositionIterator++;
				tileAnimation.Position() = position;
			}
			else if(increasePositionIterator == 102)
			{
				increasePosition = false;
				increasePositionIterator = 0;
				state = State::Solid;
			}
		}
	}

	if(tile.first == 16)
	{

	}

	FloatRect tileRect(position.first+1, position.second+1, 14, 14);

	if(e.rect->Intersects(tileRect) && state == State::Solid)
	{
		if(e.rect->Bottom >= tileRect.Top && e.prevRect->Bottom <= tileRect.Top) // Top collision
		{
			e.position.second = tileRect.Top - e.playerHeight;
			e.activateGravity = false;
			e.containsEntity = true;
			e.tileID = id;
			//printf("%s", (e.activateGravity)?"true":"false");
		}
		else if(e.rect->Top <= tileRect.Bottom && e.prevRect->Top >= tileRect.Bottom) // Bottom
		{
			e.position.second = tileRect.Bottom;
			e.velocity.second = 0;
			if(tile.first == 1)
			{
				if(e.bigMario)
				{
					image = al_create_sub_bitmap(tileSheet, 7*16, tile.second*16, 16, 16);
					state = State::Passive;
					tileAnimation.LoadContent(image, "", position);
					Sound::GetInstance().Play(4);
					//tileAnimation.SourceRect() = image;
				}
				else
				{
					increasePosition = true;
				}
			}
			else if(tile.first == 4)
			{
				image = al_create_sub_bitmap(tileSheet, 3*16, tile.second*16, 16, 16);
				tileAnimation.LoadContent(image, "", position);
				tile.first = 3;
				e.punkty += 200;
				ALLEGRO_BITMAP *tileImage = al_create_sub_bitmap(tileSheet, 16*16, tile.second*16, 16, 16);
				std::pair<float, float> newPosition (position.first, position.second - 16);
				std::pair<int, int> newTile (16, 0);
				Layer::GetInstance().AddTile(tileImage, State::Passive, newPosition, tileSheet, newTile);
				Layer::GetInstance().tiles[Layer::GetInstance().tiles.size()-1].increasePosition = true;
				Sound::GetInstance().Play(0);
				//tileAnimation.SourceRect() = image;
			}
			else if(tile.first == 7)
			{
				image = al_create_sub_bitmap(tileSheet, 3*16, tile.second*16, 16, 16);
				tileAnimation.LoadContent(image, "", position);
				tile.first = 3;
				ALLEGRO_BITMAP *tileImage = al_create_sub_bitmap(tileSheet, 20*16, tile.second*16, 16, 16);
				std::pair<float, float> newPosition (position.first, position.second - 16);
				std::pair<int, int> newTile (20, 0);
				Layer::GetInstance().AddTile(tileImage, State::Passive, newPosition, tileSheet, newTile);
				Layer::GetInstance().tiles[Layer::GetInstance().tiles.size()-1].increasePosition = true;
			}
			else if(tile.first == 8)
			{
				//9 razy mozna uderzyc
				if(collisionIterator <= 0)
				{
					image = al_create_sub_bitmap(tileSheet, 3*16, tile.second*16, 16, 16);
					tileAnimation.LoadContent(image, "", position);
					tile.first = 3;
				}
				else 
				{
					collisionIterator--;
					e.punkty += 200;
					ALLEGRO_BITMAP *tileImage = al_create_sub_bitmap(tileSheet, 16*16, tile.second*16, 16, 16);
					std::pair<float, float> newPosition (position.first, position.second - 16);
					std::pair<int, int> newTile (16, 0);
					Layer::GetInstance().AddTile(tileImage, State::Passive, newPosition, tileSheet, newTile);
					Layer::GetInstance().tiles[Layer::GetInstance().tiles.size()-1].increasePosition = true;
					Sound::GetInstance().Play(0);
				}
			}
			else if(tile.first == 9)
			{
				image = al_create_sub_bitmap(tileSheet, 3*16, tile.second*16, 16, 16);
				tileAnimation.LoadContent(image, "", position);
				tile.first = 3;
				ALLEGRO_BITMAP *tileImage = al_create_sub_bitmap(tileSheet, 10*16, tile.second*16, 16, 16);
				std::pair<float, float> newPosition (position.first, position.second - 16);
				std::pair<int, int> newTile (10, 0);
				Layer::GetInstance().AddTile(tileImage, State::Solid, newPosition, tileSheet, newTile);
				Layer::GetInstance().tiles[Layer::GetInstance().tiles.size()-1].increasePosition = true;
				Sound::GetInstance().Play(3);
				//tileAnimation.SourceRect() = image;
			}
			Sound::GetInstance().Play(2);
		}
		else if(e.rect->Right >= tileRect.Left && e.prevRect->Right <= tileRect.Left) // Left
		{
			e.position.first = tileRect.Left - 16;
			e.direction.second = e.direction.first;
			e.direction.first = Entity::Direction::Left;
		}
		else if(e.rect->Left <= tileRect.Right && e.prevRect->Left >= tileRect.Right) // Right
		{
			e.position.first = tileRect.Right;
			e.direction.second = e.direction.first;
			e.direction.first = Entity::Direction::Right;
		}

		if(tile.first == 10)
		{
			if(!e.bigMario)
			{
				image = al_create_sub_bitmap(tileSheet, 7*16, tile.second*16, 16, 16);
				state = State::Passive;
				tileAnimation.LoadContent(image, "", position);
				e.playerHeight = 32;
				std::pair<float, float> newPosition (e.position.first, e.position.second-16);
				position = newPosition;
				e.bigMario = true;
				e.punkty += 500;
				e.animation.LoadContent(e.alternativeImage, "", newPosition, "Player");
				Sound::GetInstance().Play(5);
				//e.animation.SourceRect() = e.alternativeImage;
			}
			else 
			{
				image = al_create_sub_bitmap(tileSheet, 7*16, tile.second*16, 16, 16);
				state = State::Passive;
				tileAnimation.LoadContent(image, "", position);
				e.punkty += 1500;
			}
		}
		else if(tile.first == 20)
		{
			e.punkty += 1000;
			image = al_create_sub_bitmap(tileSheet, 7*16, tile.second*16, 16, 16);
			state = State::Passive;
			tileAnimation.LoadContent(image, "", position);
			Sound::GetInstance().Play(6);
		}

		e.animation.Position() = e.position;
	}

	//if(e.containsEntity && (e.tileID == id || e.tileID == id+1))
	if(e.containsEntity && e.tileID == id)
	{
		if(e.position.first + 16 < tileRect.Left || e.position.first > tileRect.Right)
		{
			e.containsEntity = false;
			e.activateGravity = true;
		}
	}
}

void Tile::Draw(ALLEGRO_DISPLAY *display)
{
	tileAnimation.Draw(display);
}
