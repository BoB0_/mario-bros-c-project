#include "ScreenManager.h"


ScreenManager &ScreenManager::GetInstance()
{
	static ScreenManager instance;
	return instance;
}

ScreenManager::ScreenManager()
{
}


ScreenManager::~ScreenManager()
{
}

void ScreenManager::AddScreen(std::string screenName)
{
	transition.Alpha() = 0;
	fade.SetIncrease(true);
	startTransition = true;
	newScreen = screenDirectory[screenName];
	transition.IsActive() = true;
}

void ScreenManager::Initalize()
{
	screenDirectory["SplashScreen"] = new SplashScreen();
	//screenDirectory["TitleScreen"] = new TitleScreen();
	screenDirectory["GameplayScreen"] = new GameplayScreen();
	screenDirectory["GameOverScreen"] = new GameOverScreen();
	screenDirectory["TimeupScreen"] = new TimeupScreen();
	
	currentScreen = screenDirectory["GameplayScreen"];
}

void ScreenManager::LoadContent()
{
	currentScreen->LoadContent();
	transitionImage = al_load_bitmap("transitionImage.png");
	std::pair<float,float> position(0,0);
	transition.LoadContent(transitionImage, "", position);
	startTransition = false;
}

void ScreenManager::UnloadContent()
{
	al_destroy_bitmap(transitionImage);
	transition.UnloadContent();
	currentScreen->UnloadContent();

	std::map<std::string, GameScreen*>::iterator tempIterator;

	for(tempIterator = screenDirectory.begin() ; tempIterator != screenDirectory.end(); tempIterator++)
		delete tempIterator->second;

	screenDirectory.clear();
}

void ScreenManager::Update(ALLEGRO_EVENT ev)
{
	if(!startTransition)
		currentScreen->Update(ev);
	else
		Transition();
}

void ScreenManager::Draw(ALLEGRO_DISPLAY *display)
{
	currentScreen->Draw(display);
	if(startTransition)
		transition.Draw(display);
}

void ScreenManager::Transition()
{
	fade.Update(transition);
	if(transition.Alpha() >= 255)
	{
		transition.Alpha() = 255;
		currentScreen->UnloadContent();
		currentScreen = newScreen;
		currentScreen->LoadContent();
			al_rest(1.0);
	}
	else if(transition.Alpha() <= 0)
	{
		startTransition = false;
		transition.IsActive() = false;
	}
}