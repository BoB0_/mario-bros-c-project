#pragma once

#include "Layer.h"

class Map
{
public:
	Map(void);
	~Map(void);

	void LoadContent(std::string mapID);
	void UnloadContent();
	void Update(Entity &e);
	void Draw(ALLEGRO_DISPLAY *display);

private:
	//std::vector<Layer> layers;
	std::string mapID;
};