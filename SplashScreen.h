#pragma once

#include "ScreenManager.h"
#include "FadeAnimation.h"
#include "FileManager.h"
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>

class SplashScreen : public GameScreen
{
private:
	ALLEGRO_FONT *font;
	//FileManager fileManager;

	//ALLEGRO_BITMAP *image;
	//Animation fade;

	std::vector<ALLEGRO_BITMAP*> images;
	std::vector<Animation*> fade;

	//FadeAnimation fAnimation;

	int imageNumber;
public:
	SplashScreen(void);
	~SplashScreen(void);

	void LoadContent();
	void UnloadContent();
	void Update(ALLEGRO_EVENT ev);
	void Draw(ALLEGRO_DISPLAY *display);
};