#include "Player.h"

#include <iostream>
#include <string>


Player::Player(void)
{
}


Player::~Player(void)
{
}

void Player::LoadContent(std::vector<std::string> attributes, std::vector<std::string> contents, std::string type)
{
	Entity::LoadContent(attributes, contents, type);

	moveSpeed = 2;
	gravity = 1;
	jumpSpeed = 12;
	czas = 400;
	punkty = 0;
	monety = 0;
	swiat = "1-1";
	keyPressed = false;
	jump = false;
	bigMario = false;
	isDead = false;
	direction.first = Direction::Right;
	direction.second = Direction::Right;

	cameraPosition.first = 0;
	cameraPosition.second = 0;

	groundPosition.first = position.first;
	groundPosition.second = position.second;

	playerWidth = 16;
	playerHeight = 16;

	std::pair<float, float> marioPosition (32, 288);
	std::pair<float, float> swiatPosition (160, 288);
	std::pair<float, float> czasPosition (208, 288);
	std::pair<float, float> punktyPosition (32, 304);
	std::pair<float, float> swiat2Position (160, 304);
	std::pair<float, float> czas2Position (208, 304);

	MARIO.LoadContent(NULL, "MARIO", marioPosition);
	SWIAT.LoadContent(NULL, "SWIAT", swiatPosition);
	CZAS.LoadContent(NULL, "CZAS", czasPosition);
	tempString = std::to_string(long double(punkty));
	PUNKTY.LoadContent(NULL, tempString, punktyPosition);
	SWIAT2.LoadContent(NULL, swiat, swiat2Position);
	tempString = std::to_string(long double(czas));
	CZAS2.LoadContent(NULL, tempString, czas2Position);
	
	deleteTileCounter = 20;
}

void Player::UnloadContent()
{
	Entity::UnloadContent();
	animation.UnloadContent();
}

void Player::Update(ALLEGRO_EVENT ev, InputManager input, std::pair<float, float> enemyPosition)
{
	Entity::Update(ev, input, enemyPosition);
	input.Update();
	animation.IsActive() = true;

	//if(input.IsKeyDown(ALLEGRO_KEY_PAD_5))
	//{
	//	if(direction.first != Direction::Down)
	//		direction.second = direction.first;

	//	direction.first = Direction::Down;
	//}
	///*else if(input.IsKeyDown(ALLEGRO_KEY_PAD_8))
	//{
	//	direction = Direction::Up;
	//	velocity.second -= jumpSpeed;
	//}*/
	//else if(input.IsKeyDown(ALLEGRO_KEY_PAD_6))
	//{
	//	if(direction.first != Direction::Right)
	//		direction.second = direction.first;
	//	direction.first = Direction::Right;
	//	position.first += moveSpeed;
	//}
	//else if(input.IsKeyDown(ALLEGRO_KEY_PAD_4))
	//{
	//	if(direction.first != Direction::Left)
	//		direction.second = direction.first;
	//	direction.first = Direction::Left;
	//	//if(position.first > this->cameraPosition.first + 2)
	//		position.first -= moveSpeed;
	//}
	if(!isDead)
	{
		if((input.IsKeyDown(ALLEGRO_KEY_PAD_8)) && !activateGravity && !keyPressed)
		{
			jumpSpeed = 12;
			if(direction.first != Direction::Up)
				direction.second = direction.first;
		
			direction.first = direction.second;
			velocity.second = -jumpSpeed;
			activateGravity = true;
			if(!bigMario)
				Sound::GetInstance().Play(1);
			else
				Sound::GetInstance().Play(7);
		}

		if(jump && !activateGravity)
		{
			jumpSpeed = 5;
			jump = false;
			velocity.second = - jumpSpeed;
			activateGravity = true;
		}

		/*if(input.IsKeyDown(ALLEGRO_KEY_PAD_5) && bigMario && !keyPressed)
		{
			if(direction.first != Direction::Down)
				direction.second = direction.first;

			direction.first = Direction::Down;
		}*/

		if(input.IsKeyDown(ALLEGRO_KEY_PAD_6))
		{
			if(direction.first != Direction::Right)
				direction.second = direction.first;

			direction.first = Direction::Right;
			if(position.first < 3300)
				velocity.first = moveSpeed;
			else
				velocity.first = 0;
		}
		else if(input.IsKeyDown(ALLEGRO_KEY_PAD_4))
		{
			if(direction.first != Direction::Left)
				direction.second = direction.first;

			direction.first = Direction::Left;
			if(position.first > this->cameraPosition.first + 258 && position.first > 16)
				velocity.first = -moveSpeed;
			else
				velocity.first = 0;
		}
		else
		{
			velocity.first = 0;
			animation.IsActive() = false;
		}
	}

	if(activateGravity)
		velocity.second = velocity.second + gravity;
	else
		velocity.second = 0;

	position.first = position.first + velocity.first;
	position.second = position.second + velocity.second;

	animation.CurrentFrame().second = direction.first;
	animation.Position() = position;

	groundPosition.first = position.first;

	CameraUpdate(groundPosition, playerWidth, playerHeight);

	if(animation.IsActive())
		//printf("\n%g   %g   %d  %s", position.first, cameraPosition.first, tileID, (groundPosition.first - cameraPosition.first == ScreenWidth/2)?"true":"false");
		printf("\n%g\t%g\t%d\t%d", position.first, position.second, playerWidth, playerHeight);

	ssAnimation.Update(animation, direction);

	float scale = 3.0f;

	al_identity_transform(&camera);
	al_translate_transform(&camera, -(groundPosition.first+8), -(groundPosition.second+8));
	al_scale_transform(&camera, scale, scale);
	al_translate_transform(&camera, -this->cameraPosition.first + (groundPosition.first + 8), -this->cameraPosition.second + (groundPosition.second+8));
	//al_scale_transform(&camera, scale, scale);
	al_use_transform(&camera);

	delete rect;
	delete prevRect;

	rect = new FloatRect(position.first+2, position.second, 12, playerHeight);
	prevRect = new FloatRect(prevPosition.first+2, prevPosition.second, 12, playerHeight);

	MARIO.Position() = std::pair<float, float> (groundPosition.first - 112, 278);
	SWIAT.Position() = std::pair<float, float> (MARIO.Position().first + 128, 278);
	CZAS.Position() = std::pair<float, float> (MARIO.Position().first + 176, 278);
	PUNKTY.Position().first = MARIO.Position().first;
	SWIAT2.Position().first = SWIAT.Position().first;
	CZAS2.Position().first = CZAS.Position().first;
	tempString = std::to_string(long double(czas));
	CZAS2.Text() = tempString;
	tempString = std::to_string(long double(punkty));
	PUNKTY.Text() = tempString;

	keyPressed = input.IsKeyDown(ALLEGRO_KEY_PAD_8);
}

void Player::TopCollision(Entity e2)
{
	std::cout << "Top Player Collision" << std::endl;
	jump = true;
	punkty += 200;
	Sound::GetInstance().Play(9);
}

void Player::OtherCollision(Entity e2)
{
	std::cout << "Other Player Collision" << std::endl;
	if(bigMario)
	{
		Sound::GetInstance().Play(10);
		playerHeight = 16;
		animation.LoadContent(image, "", position, "Player");
		bigMario = false;
	}
	else
	{
		isDead = true;	
		/*direction.second == direction.first;
		direction.first == Direction::Down;
		ssAnimation.Update(animation, direction);*/
	}
}

void Player::CameraUpdate(std::pair<float, float> position, int playerWidth, int playerHeight)
{
	//if((-(ScreenWidth / 2) + (position.first + playerWidth / 2)) > this->cameraPosition.first )
	//if(position.first - 392 > this->cameraPosition.first)
		this->cameraPosition.first = -(ScreenWidth / 2) + (position.first + playerWidth / 2);
	this->cameraPosition.second = -(ScreenHeight / 2) + (position.second + playerHeight / 2);

	//if(this->cameraPosition.first < 0)
		//this->cameraPosition.first = 0;
	if(this->cameraPosition.second < 0)
		this->cameraPosition.second = 0;

	if(this->cameraPosition.first  + ScreenWidth > 3748)
		this->cameraPosition.first = 3748 - ScreenWidth;
}

void Player::Draw(ALLEGRO_DISPLAY *display)
{
	Entity::Draw(display);
	MARIO.Draw(display);
	SWIAT.Draw(display);
	CZAS.Draw(display);
	PUNKTY.Draw(display);
	SWIAT2.Draw(display);
	CZAS2.Draw(display);
}