#pragma once

#include <allegro5\allegro.h>
#include <iostream>
#include "Animation.h"
#include "Player.h"
#include "Sound.h"

class Tile
{
public:
	Tile(void);
	~Tile(void);

	static enum State { Solid, Passive };

	void SetContent(ALLEGRO_BITMAP *tileImage, State state, std::pair<float, float> position, ALLEGRO_BITMAP *tileSheet, std::pair<int, int> tile, int id);
	void UnloadContent();
	void Update(Entity &e);
	void Draw(ALLEGRO_DISPLAY *display);

private:
	ALLEGRO_BITMAP *image;
	ALLEGRO_BITMAP *tileSheet;
	Animation tileAnimation;
	State state;
	std::pair<float, float> position;
	std::pair<int, int> tile;
	bool containsEntity;
	bool increasePosition;
	int increasePositionIterator;
	int id;
	int collisionIterator;
};

