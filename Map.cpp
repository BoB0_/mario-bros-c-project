#include "Map.h"


Map::Map(void)
{
}


Map::~Map(void)
{
}

void Map::LoadContent(std::string mapID)
{
	this->mapID = mapID;
	/*Layer layerInstance;

	layers.push_back(layerInstance);
	layers[0].LoadContent("Layer1", this->mapID);*/

	Layer::GetInstance().LoadContent("Layer1", this->mapID);
}

void Map::UnloadContent()
{
	//layers[0].UnloadContent();
	Layer::GetInstance().UnloadContent();
}

void Map::Update(Entity &e)
{
	//layers[0].Update(e);
	Layer::GetInstance().Update(e);
}

void Map::Draw(ALLEGRO_DISPLAY *display)
{
	//layers[0].Draw(display);
	Layer::GetInstance().Draw(display);
}
