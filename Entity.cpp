#include "Entity.h"


Entity::Entity(void)
{
}


Entity::~Entity(void)
{
}

void Entity::LoadContent(std::vector<std::string> attributes, std::vector<std::string> contents, std::string type)
{
	for(int i = 0; i < attributes.size(); i++)
	{
		if(attributes[i] == "Image")
			image = al_load_bitmap(contents[i].c_str());
		else if(attributes[i] == "Position")
		{
			std::string tileString = contents[i];
			std::pair<int, int> tile;
			tile.first = atoi(tileString.substr(0, tileString.find(',')).c_str());
			tile.second = atoi(tileString.substr(tileString.find(',')+1).c_str());

			position = tile;
		}
		else if(attributes[i] == "AlternativeImage")
		{
			alternativeImage = al_load_bitmap(contents[i].c_str());
		}
		else if(attributes[i] == "Direction")
		{
			if(contents[i] == "Right")
			{
				direction.second = direction.first;
				direction.first = Direction::Right;
			}
			else if(contents[i] == "Left")
			{
				direction.second = direction.first;
				direction.first = Direction::Left;
			}
		}
		else if(attributes[i] == "MoveSpeed")
			moveSpeed = atof(contents[i].c_str());
		else if(attributes[i] == "Range")
			range = atoi(contents[i].c_str());
	}

	animation.LoadContent(image, "", position, type);

	//animation.LoadContent(image, "", position);

	rect = new FloatRect(position.first, position.second, 16, 16);
	prevRect = new FloatRect(position.first, position.second, 16, 16);
}

void Entity::UnloadContent()
{
	al_destroy_bitmap(image);
}

void Entity::Update(ALLEGRO_EVENT ev, InputManager inputManager, std::pair<float, float> enemyPosition)
{
	prevPosition = position;
}

void Entity::TopCollision(Entity e2)
{

}

void Entity::OtherCollision(Entity e2)
{

}

void Entity::Draw(ALLEGRO_DISPLAY *display)
{
	animation.Draw(display);
}