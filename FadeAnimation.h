#pragma once
#include "Animation.h"


class FadeAnimation :
	public Animation
{
public:
	FadeAnimation(void);
	~FadeAnimation(void);
	
	void Update(Animation &a);
	void SetIncrease(bool value);

private:
	float fadeSpeed;
	bool increase;
};

