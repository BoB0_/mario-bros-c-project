#pragma once

#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>
#include <allegro5\allegro_font.h>
#include <string>
#include "InputManager.h"

class Animation
{
public:
	Animation(void);
	~Animation(void);

	void LoadContent(ALLEGRO_BITMAP *image, std::string text, std::pair<float,float> position);
	void LoadContent(ALLEGRO_BITMAP *image, std::string text, std::pair<float,float> position, std::string type);
	void UnloadContent();
	virtual void Update();
	void Draw(ALLEGRO_DISPLAY *display);

	float &Alpha();
	bool &IsActive();

	std::pair<float, float> &Position();
	std::pair<int, int> &AmountOfFrames(); // amount.first = number of frames in each line, amount.second = amount of lines
	std::pair<int, int> &CurrentFrame(); // current.first = current frame in line, current.second = current line
	std::pair<int, int> FrameDimensions(); // frame.first = x dimension of frame, frame.second = y dimension of frame
	std::string &Text();

	void SetIncrease(bool value);

	ALLEGRO_BITMAP *Image();
	ALLEGRO_BITMAP *& SourceRect();

	std::string type;

private:
	ALLEGRO_BITMAP *image, *sourceRect;
	std::string text;
	ALLEGRO_FONT *font;
	std::pair<float,float> position;
	float alpha;
	bool isActive;

	float fadeSpeed;
	bool increase;

	std::pair<int, int> amountOfFrames;
	std::pair<int, int> currentFrame;
	//std::pair<int, int> frameDimensions;
};