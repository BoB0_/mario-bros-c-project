#include "Sound.h"

Sound &Sound::GetInstance()
{
	static Sound instance;
	return instance;
}


Sound::Sound()
{
}


Sound::~Sound(void)
{
}


void Sound::LoadContent(std::string fileName)
{
	fileManager.LoadContent(fileName.c_str(), attributes, contents);

	samplesReserved = 1;

	for(int i = 0; i < attributes.size(); i++)
	{
		for(int j = 0; j < attributes[i].size(); j++)
		{
			if(attributes[i][j] == "Sound")
			{
				ALLEGRO_SAMPLE *soundInstance;
				sounds.push_back(soundInstance);
				sounds[sounds.size()-1] = al_load_sample(contents[i][j].c_str());
				samplesReserved++;
			}
		}
	}

	al_reserve_samples(samplesReserved);
}

void Sound::UnloadContent()
{
	al_destroy_sample(sound);
	for(int i = 0; i < sounds.size(); i++)
	{
		al_destroy_sample(sounds[i]);
		delete sounds[i];
	}

	sounds.clear();
	attributes.clear();
	contents.clear();
}

void Sound::Update(ALLEGRO_EVENT ev)
{

}

void Sound::Play(int id)
{
	if(id == playingSample)
	{
		al_stop_sample(&sampleID);
	}
	al_play_sample(sounds[id], 1.0f, 0.0f, 1.0f, ALLEGRO_PLAYMODE_ONCE, &sampleID);
	playingSample = id;
}

/* background music

ALLEGRO_SAMPLE_INSTANCE *songInstance = al_create_sample_instance(song);
al_reserve_samples(1);
al_set_sample_instance_playmode(songInstance, ALLEGRO_PLAYMODE_LOOP);

al_attach_sample_instance_to_mixer(songInstance, al_get_default_mixer());

al_play_sample_instance(songInstance); 

al_destroy_sample_instance(songInstance);
*/
