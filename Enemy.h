#pragma once

#include "Entity.h"

class Enemy : public Entity
{
public:
	Enemy(void);
	~Enemy(void);

	void LoadContent(std::vector<std::string> attributes, std::vector<std::string> contents, std::string type);
	void UnloadContent();
	void Update(ALLEGRO_EVENT ev, InputManager inputManager, std::pair<float, float> enemyPosition);
	void TopCollision(Entity e2);
	void OtherCollision(Entity e2);
	void Draw(ALLEGRO_DISPLAY *display);

private:
	int counter;
	bool canMove;
};

