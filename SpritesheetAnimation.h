#pragma once

#include "Animation.h"

class SpriteSheetAnimation : public Animation
{
private: 
	int frameCounter;
	int direction;
public:
	SpriteSheetAnimation(void);
	~SpriteSheetAnimation(void);

	int switchFrame;

	void Update(Animation &a, std::pair<int, int> direction);
};