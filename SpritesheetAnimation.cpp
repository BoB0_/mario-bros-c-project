#include "SpriteSheetAnimation.h"


SpriteSheetAnimation::SpriteSheetAnimation(void)
{
	frameCounter = 0;
	switchFrame = 5;
}


SpriteSheetAnimation::~SpriteSheetAnimation(void)
{
}


void SpriteSheetAnimation::Update(Animation &a, std::pair<int, int> direction)
{
	if(a.IsActive())
	{
		if(a.type == "Player")
		{
			if(direction.first != 2)
			{
				frameCounter++;
				if(frameCounter >= switchFrame)
				{
					frameCounter = 0;
					a.CurrentFrame().first++;
					if(a.CurrentFrame().first * a.FrameDimensions().first >= a.FrameDimensions().first*4)
						a.CurrentFrame().first = 1;

				}
			}
			else
			{
				a.CurrentFrame().first = 6;
				a.CurrentFrame().second = direction.second;
			}
			
		}
		else if(a.type == "Enemy")
		{
			frameCounter++;
			if(frameCounter >= switchFrame)
			{
				frameCounter = 0;
				a.CurrentFrame().first++;
				if(a.CurrentFrame().first * a.FrameDimensions().first >= 32)
					a.CurrentFrame().first = 0;
			}
		}
	}
	else
	{
		if(a.type == "Player")
		{
			if(direction.first != 2)
			{
				frameCounter = 0;
				a.CurrentFrame().first = 0;
			}
			else
			{
				a.CurrentFrame().first = 6;
				a.CurrentFrame().second = direction.second;
			}
		}
		else if(a.type == "Enemy")
		{
			frameCounter = 0;
			a.CurrentFrame().first = 0;
		}
	}

	/*if(a.IsActive())
	{
			if(direction.first != 2)
			{
				frameCounter++;
				if(frameCounter >= switchFrame)
				{
					frameCounter = 0;
					a.CurrentFrame().first++;
					if(a.CurrentFrame().first * a.FrameDimensions().first >= 64)
						a.CurrentFrame().first = 1;

				}
			}
			else
			{
				a.CurrentFrame().first = 6;
				a.CurrentFrame().second = direction.second;
			}
	}
	else
	{
		if(direction.first != 2)
			{
				frameCounter = 0;
				a.CurrentFrame().first = 0;
			}
			else
			{
				a.CurrentFrame().first = 6;
				a.CurrentFrame().second = direction.second;
			}
	}*/

	a.SourceRect() = al_create_sub_bitmap(a.Image(), a.CurrentFrame().first*a.FrameDimensions().first, a.CurrentFrame().second * a.FrameDimensions().second, a.FrameDimensions().first, a.FrameDimensions().second);
}