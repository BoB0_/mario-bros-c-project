#include "GameOverScreen.h"


GameOverScreen::GameOverScreen(void)
{
}


GameOverScreen::~GameOverScreen(void)
{
}

void GameOverScreen::LoadContent()
{
	fileManager.LoadContent("Load/gameover.rb", attributes, contents);

	for(int i = 0; i < attributes.size(); i++)
	{
		for(int j = 0; j < attributes[i].size(); j++)
		{
			if(attributes[i][j] == "Image")
			{
				std::pair<float, float> position(0,0);
				images.push_back(al_load_bitmap(contents[i][j].c_str()));
				fade.push_back(new Animation);
				fade[fade.size()-1]->LoadContent(images[fade.size()-1], "", position);
				fade[fade.size()-1]->IsActive() = false;
			}
			else if(attributes[i][j] == "Text")
			{
				std::pair<float, float> position (275, 441);
				images.push_back(al_load_bitmap(contents[i][j].c_str()));
				fade.push_back(new Animation);
				std::string text = contents[i][j].c_str();
				fade[fade.size()-1]->LoadContent(NULL, text, position);
				fade[fade.size()-1]->IsActive() = true;
			}
		}
	}

	song = al_load_sample("Sounds/Game_Over.ogg");
	songInstance = al_create_sample_instance(song);
	al_set_sample_instance_playmode(songInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(songInstance, al_get_default_mixer());
	al_play_sample_instance(songInstance);
	al_identity_transform(&camera);
	al_invert_transform(&camera);
	al_translate_transform(&camera, 0,0);
	al_use_transform(&camera);
}

void GameOverScreen::UnloadContent()
{
	GameScreen::UnloadContent();
	for(int i = 0; i < fade.size(); i++)
	{
		al_destroy_bitmap(images[i]);
		delete fade[i];
	}
	fade.clear();
}

void GameOverScreen::Update(ALLEGRO_EVENT ev)
{
	fade[fade.size()-1]->Update();

	if(fade[fade.size()-1]->Alpha() == 0)
	{
		fade[fade.size()-1]->Alpha() = 255;
	}
}

void GameOverScreen::Draw(ALLEGRO_DISPLAY *display)
{
	//fade[0]->Draw(display);
	for(int i = 0; i < fade.size(); i++)
	{
		fade[i]->Draw(display);
	}
}