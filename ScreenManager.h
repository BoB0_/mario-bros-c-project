#pragma once
#include <iostream>
#include <string>
#include "GameScreen.h"
#include "FadeAnimation.h"
#include "SplashScreen.h"
#include "GameOverScreen.h"
#include "TimeupScreen.h"
//To powinno byc odkomentowane
//#include "TitleScreen.h"
#include "GameplayScreen.h"

//ALLEGRO Inits
#include <allegro5\allegro.h>
#include <allegro5\allegro_image.h>
#include <map>

#define ScreenWidth 800  // to jest rowniez w Player.h
#define ScreenHeight 608

class ScreenManager
{
private:
	ScreenManager();
	ScreenManager(ScreenManager const&);
	void operator = (ScreenManager const&);

	std::string text;
	GameScreen *currentScreen, *newScreen;

	ALLEGRO_BITMAP  *transitionImage;

	Animation transition;
	FadeAnimation fade;

	void Transition();

	bool startTransition;

	std::map<std::string, GameScreen*> screenDirectory;

public:
	~ScreenManager();
	static ScreenManager &GetInstance();

	void AddScreen(std::string newScreen);

	void Initalize();
	void LoadContent();
	void UnloadContent();
	void Update(ALLEGRO_EVENT ev);
	void Draw(ALLEGRO_DISPLAY *display);
};