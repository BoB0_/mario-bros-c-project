#pragma once

#include <allegro5\allegro5.h>
#include "InputManager.h"
#include "FileManager.h"
#include "Sound.h"

class GameScreen
{
protected: 
	InputManager input;
	std::vector<std::vector<std::string>> attributes;
	std::vector<std::vector<std::string>> contents;

	FileManager fileManager;

	ALLEGRO_SAMPLE *song;
	ALLEGRO_SAMPLE_INSTANCE *songInstance;

public:
	GameScreen();
	~GameScreen();

	virtual void LoadContent();
	virtual void UnloadContent();
	virtual void Update(ALLEGRO_EVENT ev);
	virtual void Draw(ALLEGRO_DISPLAY *display);

	InputManager GetInput();
};

