#include "Enemy.h"


Enemy::Enemy(void)
{
}


Enemy::~Enemy(void)
{
}


void Enemy::LoadContent(std::vector<std::string> attributes, std::vector<std::string> contents, std::string type)
{
	Entity::LoadContent(attributes, contents, type);
	gravity = 1;
	direction.second = direction.first;
	animation.IsActive() = true;
	ssAnimation.switchFrame = 15;
	counter = 0;
	canMove = false;
	deleteTile = false;
	isDead = false;
	deleteTileCounter = 20;
	playerWidth = 16;
	playerHeight = 16;
}

void Enemy::UnloadContent()
{
	Entity::UnloadContent();
}

void Enemy::Update(ALLEGRO_EVENT ev, InputManager inputManager, std::pair<float, float> enemyPosition)
{
	if(enemyPosition.first + 408 > position.first && !deleteTile)
	{
		prevPosition = position;

		if(direction.first == Direction::Right)
			velocity.first = moveSpeed;
		else if(direction.first == Direction::Left)
			velocity.first = -moveSpeed;

		if(activateGravity)
			velocity.second = velocity.second + gravity;
		else
			velocity.second = 0;

		position.first = position.first + velocity.first;
		position.second = position.second + velocity.second;

		animation.CurrentFrame().second = 0;
		animation.Position() = position;

		ssAnimation.Update(animation, direction);

		delete rect;
		delete prevRect;

		rect = new FloatRect(position.first+2, position.second, 12, 16);
		prevRect = new FloatRect(prevPosition.first+2, prevPosition.second, 12, 16);

		counter += abs(velocity.first);

		if(counter >= range)
		{
			counter = 0;
			if(direction.first == Direction::Right)
			{
				direction.second = direction.first;
				direction.first = Direction::Left;
			}
			else
			{
				direction.second = direction.first;
				direction.first = Direction::Right;
			}
		}
	}
	if(deleteTile)
		deleteTileCounter--;
}

void Enemy::TopCollision(Entity e2)
{
	std::cout << "Top Enemy Collision" << std::endl;
	ALLEGRO_BITMAP *tempImage = al_create_sub_bitmap(image, 2*16, 0, 16, 16);
	image = tempImage;
	animation.LoadContent(image, "", position);
	deleteTile = true;
	isDead = true;
}

void Enemy::OtherCollision(Entity e2)
{
	std::cout << "Other Enemy Collision" << std::endl;
}

void Enemy::Draw(ALLEGRO_DISPLAY *display)
{
	animation.Draw(display);
}