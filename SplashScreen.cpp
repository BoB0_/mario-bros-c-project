#include "SplashScreen.h"

SplashScreen::SplashScreen(void)
{
}


SplashScreen::~SplashScreen(void)
{
}

void SplashScreen::LoadContent()
{
	font = al_load_font("arial.ttf", 30, NULL);
	//imageNumber = 0;
	////fAnimation.Alpha() = 255;
	//fileManager.LoadContent("Load/splash.rb", attributes, contents);

	//for(int i = 0; i < attributes.size(); i++)
	//{
	//	for(int j = 0; j < attributes[i].size(); j++)
	//	{
	//		if(attributes[i][j] == "Image")
	//		{
	//			std::pair<float,float> position(0,0);
	//			images.push_back(al_load_bitmap(contents[i][j].c_str()));
	//			fade.push_back(new FadeAnimation);
	//			fade[fade.size()-1]->LoadContent(images[fade.size()-1], "", position);
	//			fade[fade.size()-1]->IsActive() = true;
	//		}
	//	}
	//}

	imageNumber = 0;
	fileManager.LoadContent("Load/splash.rb", attributes, contents);

	for(int i = 0; i < attributes.size(); i++)
	{
		for(int j = 0; j < attributes[i].size(); j++)
		{
			if(attributes[i][j] == "Image")
			{
				std::pair<float, float> position(0,0);
				images.push_back(al_load_bitmap(contents[i][j].c_str()));
				fade.push_back(new Animation);
				fade[fade.size()-1]->LoadContent(images[fade.size()-1], "", position);
				fade[fade.size()-1]->IsActive() = true;
			}
		}
	}
}

void SplashScreen::UnloadContent()
{
	GameScreen::UnloadContent();
	al_destroy_font(font);
	for(int i = 0; i < fade.size(); i++)
	{
		al_destroy_bitmap(images[i]);
		delete fade[i];
	}
	fade.clear();
}

void SplashScreen::Update(ALLEGRO_EVENT ev)
{
	fade[imageNumber]->Update();

	if(fade[imageNumber]->Alpha() == 0)
		imageNumber++;

	if(imageNumber >= fade.size() - 1 || input.IsKeyPressed(ev, ALLEGRO_KEY_ENTER))
		ScreenManager::GetInstance().AddScreen("GameplayScreen");
}

void SplashScreen::Draw(ALLEGRO_DISPLAY *display)
{
	fade[imageNumber]->Draw(display);

	//fade.Draw(display);
}